require streamdevice, 2.7.1
require ps-fug, nsenaud

## Asyn
drvAsynIPPortConfigure("HVPS", "127.0.0.1:9999")

dbLoadRecords("fughch15k100k.db")
