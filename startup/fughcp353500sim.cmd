require streamdevice, 2.7.1
require ps-fug, nsenaud

## Asyn
drvAsynIPPortConfigure("RepPS-01", "127.0.0.1:10000")
drvAsynIPPortConfigure("RepPS-02", "127.0.0.1:10004")

dbLoadRecords("fughcp353500.db")
