include $(EPICS_ENV_PATH)/module.Makefile
 
MISC      = protocol/fughch15k100k.proto
MISC     += protocol/fughcp353500.proto
STARTUPS  = startup/fughch15k100k.cmd
STARTUPS += startup/fughcp353500.cmd
STARTUPS += startup/fughch15k100ksim.cmd
STARTUPS += startup/fughcp353500sim.cmd

USR_DEPENDENCIES = streamdevice,2.7.7
